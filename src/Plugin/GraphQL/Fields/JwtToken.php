<?php

namespace Drupal\graphql_jwt\Plugin\GraphQL\Fields;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\jwt\Authentication\Provider\JwtAuth;
use Drupal\user\Entity\User;
use Drupal\user\UserAuthInterface;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * A query field that returns JWT auth token for a user.
 *
 * @GraphQLField(
 *   secure = true,
 *   id = "graphql_jwt_token",
 *   type = "JwtTokenResult",
 *   name = "JwtToken",
 *   nullable = true,
 *   multi = false,
 *   arguments = {
 *     "username" = "String!",
 *     "password" = "String!"
 *   }
 * )
 */
class JwtToken extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The Jwt Authentication provider.
   *
   * @var \Drupal\jwt\Authentication\Provider\JwtAuth
   */
  protected $auth;

  /**
   * The user authentication object.
   *
   * @var \Drupal\user\UserAuthInterface
   */
  protected $userAuth;

  /**
   * The account switcher.
   *
   * @var \Drupal\Core\Session\AccountSwitcherInterface
   */
  protected $accountSwitcher;

  /**
   * The http kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    JwtAuth $auth,
    UserAuthInterface $user_auth,
    AccountSwitcherInterface $account_switcher,
    HttpKernelInterface $http_kernel
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->auth = $auth;
    $this->userAuth = $user_auth;
    $this->accountSwitcher = $account_switcher;
    $this->httpKernel = $http_kernel;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('jwt.authentication.jwt'),
      $container->get('user.auth'),
      $container->get('account_switcher'),
      $container->get('http_kernel')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    $content = [
      'name' => $args['username'],
      'pass' => $args['password'],
    ];

    // Create request against /user/login requiring JSON output.
    $request = Request::create('/user/login?_format=json', 'POST', [], [], [], [], json_encode($content));
    // Make the subrequest to authenticate user the same way as Drupal does.
    $response = $this->httpKernel->handle($request, KernelInterface::SUB_REQUEST);
    if ($response && $json = json_decode($response->getContent(), TRUE)) {
      $uid = $json['current_user']['uid'] ?? 0;
      if ($uid && $user = User::load($uid)) {
        // Switch to authenticated account, since JWT token generation is based
        // on current user. See JwtAuthIssuerSubscriber::setDrupalClaims().
        $this->accountSwitcher->switchTo($user);
        $token = $this->auth->generateToken();
        // Switch back to original account.
        $this->accountSwitcher->switchBack();

        if ($token !== FALSE) {
          $return = [
            'type' => 'UserTokenResult',
            'jwt' => $token,
          ];
          yield $return;
        }
      }
    }
    \Drupal::logger('jobiqo_auth')->error('Error logging user.');
  }

}
